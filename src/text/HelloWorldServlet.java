package text;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.html.HtmlWriter;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.rtf.RtfWriter2;

import jdk.nashorn.internal.ir.debug.JSONWriter;
import jdk.nashorn.internal.parser.JSONParser;
import jdk.nashorn.internal.runtime.JSONFunctions;
import jdk.nashorn.internal.runtime.JSONListAdapter;



public class HelloWorldServlet extends HttpServlet {
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
		      throws IOException, ServletException {
System.out.println("servlet start ....");
		  String presentationtype = request.getParameter("presentationtype");

		    Document document = new Document();
		    try {
		      if ("pdf".equals(presentationtype)) {
		        response.setContentType("application/pdf");
		        PdfWriter.getInstance(document, response.getOutputStream());
		      } else if ("html".equals(presentationtype)) {
		        response.setContentType("text/html");
		        HtmlWriter.getInstance(document, response.getOutputStream());
		      } else if ("rtf".equals(presentationtype)) {
		        response.setContentType("text/rtf");
		        RtfWriter2.getInstance(document, response.getOutputStream());
		        
		      } else if ("json".equals(presentationtype)) {
			        response.setContentType("application/json");
			        response.setCharacterEncoding("UTF-8");
			       

			      } 

		      document.open();

		      document.add(new Paragraph("Hello World"));
		      document.add(new Paragraph(new Date().toString()));
		    } catch (DocumentException de) {
		      de.printStackTrace();
		      System.err.println("document: " + de.getMessage());
		      System.out.println("document: " + de.getMessage());
		    }

		    document.close();
		  }
	 
	 
	 
	 
	 
	 
	 
}


/*
 * gedt mime type use 
  
  String urltext = "http://www.vogella.com";
 
URL url = new URL(urltext);
String contentType = ((HttpURLConnection) url.openConnection())
    .getContentType();
System.out.println(contentType);
*/